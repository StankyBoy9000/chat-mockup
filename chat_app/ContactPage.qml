import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

Page {

    id: root
    header: ChatToolBar {
            Label {
                text: qsTr("Contacts")
                font.pixelSize: 20
                anchors.centerIn: parent
            }
        }

        ListView {
            id: listView
            anchors.rightMargin: 0

            anchors.fill:parent
            topMargin: 48
            leftMargin: 48
            bottomMargin:48
            rightMargin:48
            spacing: 20

            model: ["Jelena Stanisic", "Marko Stanojevic", "Djordje Jotic"]
            delegate: ItemDelegate {
                id: delegate
                text: modelData
                width: ListView.view.width - ListView.view.leftMargin - ListView.view.rightMargin
                height: avatar.implicitHeight
                leftPadding: avatar.implicitWidth + 32
                onClicked: root.StackView.view.push("qrc:/ConversationPage.qml", { inConversationWith: modelData })

                Image{

                    id: avatar
                    source: "qrc:/images/" + modelData.replace(" ", "_") + ".png"
                }


                Rectangle {
                     id: rect
                     width: 8
                     height: width
                     color: randomActivity()
                     border.color: rect.color
                     border.width: 0.5
                     radius: width*0.5
                     anchors.bottom: avatar.bottom
                     anchors.left: avatar.right
                     anchors.margins: 35

                     function randomNumber(){
                         var number = (Math.random() * 5000)
                         console.log(number)
                         return number
                     }

                     function randomActivity(){
                         var color = "green"
                         var number = Math.random()*10

                         color = number > 5 ? "green" : "orange"
                         return color
                     }

                     Text {
                          anchors.left: parent.right
                          leftPadding: 8
                          color: Qt.colorEqual(parent.color, "orange") ? "orange" : "green"
                          text: Qt.colorEqual(parent.color, "orange") ? "Offline" : "Online"
                     }
                }

            }

        }

    }


