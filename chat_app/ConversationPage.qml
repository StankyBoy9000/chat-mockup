import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12


Page {

    Timer {
            interval: randomNumber()
            running: true
            repeat: true
            onTriggered: messageModel.randomMessage()
        }
    function randomNumber(){
        var number = (Math.random(1) * 15000)
        console.log(number)
        return number
    }



    id: root

    property string inConversationWith

    header: ChatToolBar {
            ToolButton {
                text: qsTr("Back")
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter
                onClicked: root.StackView.view.pop()
            }

            Label {
                id: pageTitle
                text: inConversationWith
                font.pixelSize: 20
                anchors.centerIn: parent
            }
        }

    ColumnLayout {
        anchors.fill: parent
        ListModel{
            id: messageModel

            function randomSentence(){
                var sentences= [
                    'Hej',
                    'Kako si?',
                    'Nije sta si rekao. Vec kako si to rekao.',
                    'Gde si bio do sad?',
                    'Jel idemo na tenis?',
                    'Halo? Zasto mi ne odgovaras?',
                    'Hajde do centra.',
                    'Koliko casova imamo danas?',
                    'Posalji mi lokaciju.',
                    'Kupio sam novi komp. :)',
                    'Ovo je random poruka.',
                    'Dosta mi je.',
                    'Prejeo sam se.'
                ]
                var index= Math.floor(Math.random() * (sentences.length - 1));
                return sentences[index];
            }

            function randomMessage(){
                messageModel.append({"messageText": randomSentence(),"sender": inConversationWith});}


        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: pane.leftPadding + messageField.leftPadding
            displayMarginBeginning: 40
            displayMarginEnd: 40
            verticalLayoutDirection: ListView.TopToBottom
            spacing: 12
            model: messageModel
            delegate: Row {
                id: messageRow
                readonly property bool sentByMe: sender == "Me"

                anchors.right: sentByMe ? parent.right : undefined
                spacing: 6

                Rectangle {
                    id: avatar
                    width: height
                    height: parent.height
                    color: "grey"
                    visible: !sentByMe
                    Image{

                        height: parent.height
                        width: height
                        id: senderImg
                        source: "qrc:/images/" + inConversationWith.replace(" ", "_") + ".png"
                    }
                }

                Rectangle {
                    width: Math.min(messageLabel.implicitWidth + 24)
                    height: 40
                    color: sentByMe ? "lightgrey" : "steelblue"

                    Label {
                        id: messageLabel
                        anchors.centerIn: parent
                        text: messageText
                        color: sentByMe ? "black" : "white"
                    }
                }
            }

            ScrollBar.vertical: ScrollBar {}
        }

        Pane {
            id: pane
            Layout.fillWidth: true

            RowLayout {
                width: parent.width

                TextArea {
                    id: messageField
                    Layout.fillWidth: true
                    placeholderText: qsTr("Send a message to " + inConversationWith)
                    wrapMode: TextArea.Wrap
                }

                Button {
                    id: sendButton
                    text: qsTr("Send")
                    enabled: messageField.length > 0
                    onClicked: { messageModel.append({"messageText": messageField.text,"sender": "Me"});
                                 messageField.text = "";

                    }
                }
            }
        }
    }
}
